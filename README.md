# Documentation generator for DS instance of Adullact

## Source 

https://github.com/betagouv/doc.demarches-simplifiees.fr

>  "Read-only" repository. Changes are done mostly by non-devs, from the gitbook interface. And then there is a sync from gitbook to this github repository

## TODO 

- [ ] identify problems (some syntaxes specific to gitbook in markdown files)
- [ ] automate extraction of files that interest us and clean them up (domain name, emails, ...)
- [ ] generate a static site with Hugo software and a 100% accessible template!
